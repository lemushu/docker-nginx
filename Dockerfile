FROM debian:jessie
MAINTAINER mushu "loicmartinez.lyon@gmail.com"

RUN apt-get update && apt-get install -y nginx

ADD nginx.conf /etc/nginx/
ADD gigaboard.conf /etc/nginx/sites-available/

RUN ln -s /etc/nginx/sites-available/gigaboard.conf /etc/nginx/sites-enabled/gigaboard
RUN rm /etc/nginx/sites-enabled/default

RUN echo "upstream php-upstream { server phpstream:9000; }" > /etc/nginx/conf.d/upstream.conf

RUN usermod -u 1000 www-data

CMD ["nginx"]

EXPOSE 80
EXPOSE 443
